const themeCookieName = 'theme';
const themeDark = 'dark';
const themeLight = 'light';

const primaryColor = '#4834d4';
const WarningColor = '#f0932b';
const successColor = '#6ab04c';
const dangerColor = '#eb4d4b';

const body = document.getElementsByTagName('body')[0];
const menuBar = document.querySelector('.navbar .navbar-nav .nav-item .nav-item__link i');
const switchThemeDark = document.querySelector('#switch-theme');
// const collapSidebar = document.querySelector('#collapse-sidebar');

function setCookie(cname, cvalue, exdays) {
    var d = new Date()
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 60 * 1000))
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + "jpath=/"
}

// const nameFunction = (adsf) => {

// }

const getCookie = (cname) => {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const loadTheme = () => {
    let theme = getCookie(themeCookieName)
    body.classList.add(theme === "" ? themeLight : theme)
}
loadTheme()

const switchTheme = () => {
    if (body.classList.contains(themeLight)) {
        body.classList.remove(themeLight);
        body.classList.add(themeDark);
        setCookie(themeCookieName, themeDark)
    } else {
        body.classList.remove(themeDark);
        body.classList.add(themeLight);
        setCookie(themeCookieName, themeLight)

    }
}

switchThemeDark.addEventListener('click', () =>{
    switchTheme();
})

menuBar.addEventListener('click', function () {
    body.classList.toggle('sidebar-expand');
})

// function collapseSidebar(){
//     body.classList.toggle('sidebar-expand');
// }
// collapSidebar.addEventListener('click', function() {
//     collapseSidebar();
// })


window.onclick = function (event) {
    openCloseDropdown(event);

}
function closeAllDropdown() {
    let dropdowns = document.getElementsByClassName('dropdown-expand');
    for (let i = 0; i < dropdowns.length; i++) {
        dropdowns[i].classList.remove('dropdown-expand');
    }
}
function openCloseDropdown(event) {
    // console.log(envent);
    if (!event.target.matches('.dropdown-toggle')) {
        //
        // Close dropdown when click out of dropdown menu
        //
        closeAllDropdown();
    } else {
        var toggle = event.target.dataset.toggle;
        // console.log(toggle);
        var content = document.getElementById(toggle);
        if (content.classList.contains('dropdown-expand')) {
            closeAllDropdown();
        } else {
            closeAllDropdown();
            content.classList.add('dropdown-expand');
        }
    }
}

var ctx = document.getElementById('myChart');
ctx.height = 500;
ctx.width = 500;
var data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    datasets: [{
        fill: false,
        label: 'Completed',
        borderColor: successColor,
        data: [120, 115, 130, 100, 123, 88, 99, 66, 120, 52, 59, 110],
        borderWidth: 2,
        lineTension: 0,
    }, {
        fill: false,
        label: 'Issues',
        borderColor: dangerColor,
        data: [66, 44, 12, 48, 99, 56, 78, 23, 100, 22, 47, 11],
        borderWidth: 2,
        lineTension: 0,
    }]
}
var lineChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        maintainAspectRatio: false,
        bezierCurve: false,
    }
})